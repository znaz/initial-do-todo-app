const mongoose = require("mongoose");

const todoSchema = new mongoose.Schema({
  title : {
    type : String,
    required : true
  },
  user : {
    type : mongoose.ObjectId,
    ref : 'User'
  },
  date : {
    type : Date,
    default : Date.now
  },
  done : {
    type : Boolean,
    default : false
  }
});

const Todo = mongoose.model("Todo", todoSchema);

module.exports = Todo;
