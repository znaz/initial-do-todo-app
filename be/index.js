const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const cookieParser = require('cookie-parser')
const port = 3000;
require("dotenv").config();

app.use(express.json());
app.use(cors({
    origin : ["http://localhost:5173","https://65c48786f18c1f4451ab7cb3--tubular-kitsune-731913.netlify.app"],
    methods : ["POST","GET","PATCH","DELETE"],
    credentials : true
}));
app.use(cookieParser())


const User = require("./Routes/user");
const Todo = require("./Routes/todos")


app.use("/users", User);
app.use("/todos", Todo)



app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

main()
  .then((res) => console.log("db connected"))
  .catch((err) => console.log(err));

async function main() {
  const dbUrl = process.env.DB_URL;
  const dbPassword = process.env.DB_PASSWORD;
  const UrlWithPassword = dbUrl.replace("<password>", dbPassword);
  await mongoose.connect(UrlWithPassword);

  // use `await mongoose.connect('mongodb://user:password@127.0.0.1:27017/test');` if your database has auth enabled
}
