const express = require('express')
const router = express.Router()
const User = require('../Models/user')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;





  router.post('/verify', async (req,res,next)=>{
    try{
        const token = req.cookies.token
        if(!token){
            return res.json({message : "User not logged in"})
        }
        return res.send("Logged in")
    }
    catch(err){
        res.status(500).send("Error Occured")
    }
  })
  router.post('/signup', async(req, res,next) => {
    try{
        const hash = bcrypt.hashSync(req.body.password, saltRounds);
        const user = new User({...req.body, password:hash})
        await user.save()

        res.status(201).json(user)
    }
    catch(err){
        res.status(500).send("Error Occured")
    }
  })

  router.post('/login', async(req,res,next)=>{
    try{
        const {email,password} = req.body

        const user = await User.findOne({email})
        
        if(!user){
            return res.json({message :'Incorrect password or email'}) 
        }
        const auth = await bcrypt.compareSync(password, user.password);
        if(!auth){
            return res.json({message :'Incorrect password or email'}) 
        }
        const token = jwt.sign({ name: user.name, id : user._id }, process.env.TOKEN,{ expiresIn: '1800s' })
        
        res.cookie("token",token,{
            withCredentials: true,
            httpOnly: false,
        })
        res.status(201).json({user:{name : user.name, id : user._id}});
    }
    catch(err){
        res.status(500).send("Error Occured")
    }
  })
  router.post('/logout', async(req,res,next)=>{
    try{
        res.cookie('token', "", {expires: new Date(0)})
        res.status(204).send("Logout successfully")
    }
    catch(err){
        res.status(500).send("Error Occured")
    }
  })
  
  
  module.exports = router