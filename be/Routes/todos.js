const express = require('express')
const Todo = require('../Models/todo')
const router = express.Router()
const moment = require("moment-timezone");
const jwt = require('jsonwebtoken');



const verifyLogin = (req, res, next)=>{
    try{
        const token = req.cookies.token
        
        if(!token){
           return res.status(401).send("Unauthorized Login")
        }
        try {
            const decoded = jwt.verify(token, process.env.TOKEN);
            req.user = decoded.id
          } catch(err) {
            
            return res.status(401).send("Unauthorized Login")
            console.log(err)
          }
        next()
    }
    catch(err){
        res.status(500).send("Internal Error")
        console.log(err)
    }
    
    
}

router.get("/", async(req,res,next)=>{
    try{   
        const todos = await Todo.find({user : req.user})

        res.status(200).json(todos)
    }
    catch(err){
        res.status(500).send({message: "invalid login"})
        console.log(err)
    }
})

router.post('/',async(req,res,next)=>{
    try{
        
        const user = req.user
        
        const currentDate = moment().tz('Asia/Kolkata');

        const todo = new Todo({...req.body,user, date:currentDate})
        await todo.save()
        
       

        res.status(201).json({
            ...todo.toObject(),
            date: currentDate.format("YYYY-MM-DDTHH:mm:ss")
        })
    }
    catch(err){
        res.status(500).send("Error Occured")
        console.log(err)
    }
  })

  router.delete('/:todoId',async(req,res,next)=>{
    try{
        
        
        const todo = await Todo.findById(req.params.todoId)
        if(!todo){
            res.status(400).json({ error: 'Invalid Todo ID' });
        } else {
            await Todo.findByIdAndDelete(todo)
            res.status(200).json({message:"Deleted"})  
        }
                  
        
    }
    catch(err){
        res.status(500).send("Error Occured")
        
    }
  })

  router.patch('/:todoId',async(req,res,next)=>{
    try{
        const todoId = req.params.todoId
        const {done} = req.body

        const todo = await Todo.findById(todoId);

        todo.done = !todo.done

        const updatedTodo = await todo.save();

        res.json(updatedTodo);
    }
    catch(err){
        res.status(500).send("Error Occured")
        
    }
  })
  
  module.exports = router