import { object, string } from 'yup';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { url } from '../url';

const userSchema = object({
    name: string().required(),
    email: string().email().required(),
    password: string().required(),

})


function SignupPage() {

const navigate = useNavigate()


    const handleClick = async(e)=>{
        e.preventDefault()
        const form = e.target

        const name = form['name'].value
        const email = form['email'].value
        const password = form['password'].value
        const confirmPassword = form['confirmPassword'].value

        await userSchema.validate( {
            name,
            email,
            password,
        });


        if(password === confirmPassword)
        {

            axios.post(url+'/users/signup',{
                name,
                email,
                password
            })
            .then((res)=>{
                console.log(res)
                navigate("/login")
            })

            .catch(err=> console.log(err))
        }
        
        
        
    }
  return (
    <main className="flex flex-row justify-center items-center h-screen">
        <form onSubmit={handleClick} className=" md:w-3/6 font-semibold flex flex-col justify-center p-16 border-2 rounded-tl-2xl rounded-br-2xl border-purple-950">
            <div className="flex flex-col ">
                <label htmlFor="name">Name</label>
                <input className="p-3 mb-4 mt-2  border-2" type="text" id='name' />
                
            </div>
            <div className="flex flex-col ">
                <label htmlFor="email">Email</label>
                <input className="p-3 mb-4 mt-2  border-2" type="email" id='email' />
            </div>
            <div className="flex flex-col ">
                <label htmlFor="password">Password</label>
                <input className="p-3 mb-4 mt-2  border-2" type="password" id='password' />
            </div>
            <div className="flex flex-col ">
                <label htmlFor="confirmPassword">Confirm Password</label>
                <input className="p-3 mb-4 mt-2  border-2" type="password" id='confirmPassword' />
            </div>

            <button type='submit' className="mt-8 bg-purple-800 rounded-sm py-2  w-24 text-white self-center">SIGN UP</button>
        </form>
    </main>
  )
}

export default SignupPage