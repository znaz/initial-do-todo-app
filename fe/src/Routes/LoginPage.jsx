import { object, string } from "yup";
import axios from "axios";
import { useNavigate, Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addUser } from "../features/auth/authSlice";
import { useState } from "react";
import { url } from "../url";

const userSchema = object({
  email: string().email().required(),
  password: string().required(),
});

function LoginPage() {
  const [error, setError] = useState("");

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleClick = async (e) => {
    e.preventDefault();
    const form = e.target;

    const email = form["email"].value;
    const password = form["password"].value;

    await userSchema.validate({
      email,
      password,
    });

    axios
      .post(
        url + "/users/login",
        {
          email,
          password,
        },
        { withCredentials: true }
      )
      .then((res) => {
        const user = res.data.user;
        const errr = res.data.message;
        dispatch(addUser(user));
        if (user) {
          navigate("/todos");
        } else {
          setError(errr);
        }
      })

      .catch((err) => console.log(err));
  };
  return (
    <main className="flex flex-row justify-center items-center h-screen">
      <form
        onSubmit={handleClick}
        className=" md:w-3/6 font-semibold flex flex-col justify-center p-16 border-2 rounded-tl-2xl rounded-br-2xl border-purple-950"
      >
        <div className="flex flex-col ">
          <label htmlFor="email">Email</label>
          <input className="p-3 mb-4 mt-2  border-2" type="email" id="email" />
        </div>
        <div className="flex flex-col">
          <label htmlFor="password">Password</label>
          <input
            className=" p-3 mb-4 mt-2  border-2"
            type="password"
            id="password"
          />
        </div>

        {error ? (
          <span className="text-center bg-red-100 text-red-800 p-2 mt-6">
            {error}!
          </span>
        ) : (
          ""
        )}

        <button
          type="submit"
          className="mt-8 bg-purple-800 rounded-sm py-2  w-24 text-white self-center"
        >
          LOGIN
        </button>

        <span className="mt-12 text-center">
          Dont have an account!{" "}
          <Link
            to="/signup"
            className="underline underline-offset-1 text-purple-800"
          >
            {" "}
            Signup
          </Link>
        </span>
      </form>
    </main>
  );
}

export default LoginPage;
