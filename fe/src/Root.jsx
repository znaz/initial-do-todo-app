import { Link, Outlet,useNavigate} from "react-router-dom";
import Nav from "./Components/Nav/Nav";
import axios from 'axios';
import { useDispatch, useSelector } from "react-redux";
import { removeUser } from './features/auth/authSlice';
import { useState } from "react";
import { url } from "./url";

function App() {
  const [drawerVisible,setDrawerVisible] = useState(false)
  const dispatch = useDispatch()
  const user = useSelector(state=>state.auth.user)

  const navigate = useNavigate()
  const handleLogout = ()=>{
    axios.post(url+"/users/logout", {}, {withCredentials : true})
    .then(res=>{
      console.log(res)
      dispatch(removeUser())
      navigate("/login")
    })
    .catch(err=>console.log(err))
  }
  
  return <>
  <div className="relative">
  <div className={`absolute z-10  top-0 right-0 w-3/5 h-full ${drawerVisible? "translate-x-0" : "translate-x-full"  } transition-all duration-300  bg-white `}>
  {user? <div className=" ml-4 mt-4 flex flex-row items-center gap-2"><span className=" font-bold text-xl bg-purple-800 w-6 h-6 rounded-full flex justify-center items-center text-white p-4">{user.name.charAt(0)} </span> <span className="font-semibold text-lg ">{user.name}</span></div>: ""}
    
    <button onClick={()=>setDrawerVisible(false)}> 
      <img className="w-6 fixed right-3 top-3" src="icons/close.svg" alt="" />
    </button>
  <Link to="/" className="flex flex-row items-center justify-center shadow-lg p-4 gap-4 mb-2 ">
    <img className="h-12  " src="images/todo.png" alt="initail do logo" />
    <span className="text-xl font-bold text-purple-950">INITIAL DO</span>
    </Link>
      <Nav />
      {user? <div className="shadow-lg w-full p-6 text-center font-semibold "> <span onClick={handleLogout}>Logout</span></div> : <div className="font-semibold shadow-lg w-full p-6 text-center  "><Link className="hover:text-orange-600" to="/login">Login</Link></div> }
  </div>
  
  <header className="shadow-lg flex flex-row justify-between items-center  px-6" >
    
    
    <Link to="/" className="flex flex-row items-center gap-4">
    <img className="h-16  " src="images/todo.png" alt="" />
    <span className="text-xl font-bold text-purple-950">INITIAL DO</span>
    </Link>
    <div className="hidden md:flex max-w-sm w-full">
      <Nav/>

    </div>
    
    <div className="flex flex-row items-center gap-4 justify-center">
    <div className="hidden md:block">{user? <div className="flex flex-row items-center gap-3 "><span className="bg-purple-600 w-8 h-8 font-bold text-white text-xl rounded-full flex  items-center justify-center">{user.name.charAt(0)}</span> <button onClick={handleLogout} className="flex flex-col items-center justify-center text-sm font-semibold"><img className="w-6 " src="/icons/logout.svg" alt="" /> </button></div> :<Link className="font-semibold hover:text-orange-600" to="/login">Login</Link>}</div>
    <button className="block md:hidden" onClick={()=>{setDrawerVisible(true)}}>
      <img className="w-8" src="icons/menu.svg" alt="" />
    </button>
    </div>
  </header>
  
  <Outlet/>


  <footer className=" py-8 text-white bg-purple-950 flex flex-row justify-between items-center px-12 font-semibold text-sm ">
    <span> &copy;InitialDo</span>
    <span>All rights reserved</span>
  </footer>
  </div>
  </>;
}

export default App;
