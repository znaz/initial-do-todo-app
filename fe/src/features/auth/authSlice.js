import { createSlice } from '@reduxjs/toolkit'

const storedUserData = localStorage.getItem('user');
let initialUser = null;

try {
  initialUser = storedUserData ? JSON.parse(storedUserData) : null;
} catch (error) {
  console.error('Error parsing user data from localStorage:', error);
}

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    user:  initialUser,
  },
  reducers: {
    addUser : (state,action)=>{
        state.user = action.payload
        localStorage.setItem('user', JSON.stringify(action.payload));
    },
    removeUser: (state) => {
      state.user = null;

      localStorage.removeItem('user');
    },
  }
})



export const {addUser, removeUser} = authSlice.actions


export default authSlice.reducer